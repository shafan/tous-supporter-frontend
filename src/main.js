import Vue from 'vue'
import './plugins/axios'
import './plugins/vuetify'
import App from './App.vue'
import 'fullpage.js/vendors/scrolloverflow' // Optional. When using scrollOverflow:true
import VueFullPage from 'vue-fullpage.js'
import 'fullpage.js/dist/fullpage.css'

Vue.use(VueFullPage);

Vue.config.productionTip = process.env.NODE_ENV === 'production'

window.eventBus = new Vue()

new Vue({
  render: h => h(App)
}).$mount('#app')
