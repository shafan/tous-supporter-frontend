import axios from 'axios';

const ENDPOINT_REVERSE = 'https://nominatim.openstreetmap.org/reverse';
const ENDPOINT = 'https://nominatim.openstreetmap.org/search';
const FORMAT = 'jsonv2';

export function currentCoordinates() {
    return new Promise((resolve, reject) => {
        navigator.geolocation.getCurrentPosition(
            ({ coords }) => resolve(coords),
            // Reject if the user doesn't
            // allow accessing their location.
            error => reject(error),
        );
    });
}

export async function addressByCoordinates({ latitude, longitude }) {
    const { data } = await axios.get(ENDPOINT_REVERSE, {
        params: {
            format: FORMAT,
            lat: latitude,
            lon: longitude,
        },
    });

    let address = data.address;
    address.lat = data.lat;
    address.lon = data.lon;

    return address;
}

export async function currentAddress() {
    const coordinates = await currentCoordinates();

    return addressByCoordinates(coordinates);
}

export async function fetchAddress(input_address) {
    const address = await addressByName(input_address);

    return address;
}

export async function addressByName(input_address) {
    const { data } = await axios.get(ENDPOINT, {
        params: {
            format: FORMAT,
            q: input_address,
            limit: 1,
            addressdetails: 1,
        },
    });

    let first_address = data[0];


    let address = first_address.address;
    address.lat = first_address.lat;
    address.lon = first_address.lon;

    return address;
}
